<?php
/**
 * The template for displaying author pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Simple_Nineteen
 * @since 1.1
 * @version 1.1
 */

get_header(); ?>

<div class="wrap">

	<?php if ( have_posts() ) : ?>
		<header class="page-header simplenineteen-author">
			<div class="container">
			<div class="avatar">
			<?php
				echo get_avatar( get_the_author_meta( 'ID' ) );
			?>
			</div>
			<div class="bio">
			<h1 class="page-title">
			<?php
				the_author();
			?>
			</h1>
			<?php
				the_archive_description( '<div class="taxonomy-description">' );
				if ( ! get_the_author_meta( 'user_url' ) == "" ) { ?>
				<div class="website"><a href="<?php echo get_the_author_meta( 'user_url' ); ?>" class="link">
				<?php echo simplenineteen_get_svg( array( 'icon' => 'globe' ) );?>
				<span class="link-text"><?php echo get_the_author_meta( 'user_url' ); ?></span> 
				</a></div>
				<?php
				}
			?>
			</div>
			</div>
			</div>
		</header><!-- .page-header -->
	<?php endif; ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) :
			?>
			<h1 class="page-title posts"><?php _e( 'Posts:', 'simplenineteen' ); ?></h1>
			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/post/content', get_post_format() );

			endwhile;

			the_posts_pagination(
				array(
					'prev_text'          => simplenineteen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'simplenineteen' ) . '</span>',
					'next_text'          => '<span class="screen-reader-text">' . __( 'Next page', 'simplenineteen' ) . '</span>' . simplenineteen_get_svg( array( 'icon' => 'arrow-right' ) ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'simplenineteen' ) . ' </span>',
				)
			);

		else :

			get_template_part( 'template-parts/post/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php
get_footer();
