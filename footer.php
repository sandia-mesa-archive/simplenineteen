<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Simple_Nineteen
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->

<?php
if ( !simplenineteen_is_fullscreen() ) {
?>

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="wrap">
				<?php
				get_template_part( 'template-parts/footer/footer', 'widgets' );
				
				get_template_part( 'template-parts/footer/site', 'info' );
				
				?>
			</div><!-- .wrap -->
		</footer><!-- #colophon -->
<?php
}
?>
	</div><!-- .site-content-contain -->
</div><!-- #page -->

<?php 
if(is_single()){
	get_template_part( 'template-parts/footer/modal', 'social' );
}
?>

<?php wp_footer(); ?>

</body>
</html>
