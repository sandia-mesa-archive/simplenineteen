<?php
/**
 * Displays social share bar
 *
 * @package Simple_Nineteen
 * @since 1.0
 * @version 1.3
 */

?>
<div class="share">
	<div class="header"><?php esc_attr_e( 'Share via:', 'simplenineteen' ); ?></div>
	<?php $platforms = simplenineteen_share_platforms();
	$j = 0;
	for ( $i = 0; $i < count( $platforms ); $i++ ) {
		if (simplenineteen_get_share_option( $platforms[$i]['slug'] ) == '1') {
			if ( $j > 0 ) { ?> <span>|</span><?php }
			$popup = $platforms[$i]['popup']; ?>
			<a class="sbtn s-<?php echo $platforms[$i]['slug']; ?><?php if ($popup == true) { echo ' popup-trigger'; } ?>"
				<?php if ($popup == true ) { ?>
					data-popup-trigger="<?php echo $platforms[$i]['slug']; ?>"
				<?php } ?> data-url-prefix="<?php echo $platforms[$i]['url_before_args']; ?>"
				<?php if ( isset($platforms[$i]['before_url_param'])) { ?> data-before-url-param="<?php echo $platforms[$i]['before_url_param']; ?>"<?php }
					if ( $platforms[$i]['hashtags'] == true ) { ?> hashtags="hashtags"<?php }
					if ( $platforms[$i]['author'] == true ) { ?>authors="authors"<?php }
					if ( isset($platforms[$i]['title_in_link']) && $platforms[$i]['title_in_link'] == true) { ?>
						title-in-link="title-in-link"
					<?php } ?>><?php esc_attr_e( $platforms[$i]['title'], 'simplenineteen' ); ?></a><?php
				$j += 1;
		}
	} ?>
</div>