<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Simple_Nineteen
 * @since 1.0
 * @version 1.0
 */

$section_header = is_404() ? 'error-404 not-found' : 'no-results not-found';
$page_title = is_404() ? 'Oops! That page can&rsquo;t be found.' : 'Nothing Found';
$page_content = is_404() ? 'It looks like nothing was found at this location. Maybe try a search?' : 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.';
$page_section_end = is_404() ? '<!-- .error-404 -->' : '<!-- .no-results -->';
?>

<section class="<?php echo $section_header; ?>">
	<header class="page-header">
		<h1 class="page-title"><?php _e( $page_title, 'simplenineteen' ); ?></h1>
	</header>
	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) :
			?>

			<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'simplenineteen' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php else : ?>

			<p><?php _e( $page_content, 'simplenineteen' ); ?></p>
			<?php
				get_search_form();

		endif;
		?>
	</div><!-- .page-content -->
</section><?php echo $page_section_end; ?>
