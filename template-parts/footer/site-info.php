<?php
/**
 * Displays footer site info
 *
 * @package Simple_Nineteen
 * @since 1.0
 * @version 1.0
 */

?>
<?php
if ( is_active_sidebar( 'site-info' ) ) { ?>
<div class="site-info">
<?php
		if ( is_active_sidebar( 'site-info' ) ) {
			?>
			<div class="widget-column footer-widget-1">
				<?php dynamic_sidebar( 'site-info' ); ?>
			</div>
			<?php
		}
		?>
</div><!-- .site-info -->
<?php } ?>
