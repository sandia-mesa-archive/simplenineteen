<?php
/**
 * The template for displaying the social modal
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Simple Nineteen
 * @since 1.0
 * @version 1.3
 */

 $platforms = simplenineteen_share_platforms();

 for ( $i = 0; $i < count( $platforms ); $i++) {
   if ( $platforms[$i]['popup'] == true && simplenineteen_get_share_option( $platforms[$i]['slug'] ) == '1' ) {
     ?>
     <div
       class="popup-modal shadow"
       data-popup-modal="<?php echo $platforms[$i]['slug']; ?>">
       <a class="popup-modal__close">&times;</a>
       <h3 class="font-weight-bold">
        <?php printf( __( 'Enter your %s instance&rsquo;s address:', 'simplenineteen' ), $platforms[$i]['title'] ); ?>
       </h3>
       <p class="example-text">
       <?php printf( __( '(ex: %s)', 'simplenineteen' ), $platforms[$i]['example'] ); ?>
       </p>
       <input type="text" class="<?php echo $platforms[$i]['slug']; ?> popup-modal__instance"></input>
       <input type="checkbox" class="<?php echo $platforms[$i]['slug']; ?> popup-modal__remember">
       <?php esc_attr_e( 'Remember my instance (This is saved to your browser&rsquo;s local storage only)', 'simplenineteen' ); ?></input>
       <br><br>
       <p class="<?php echo $platforms[$i]['slug']; ?> error">
       <?php printf( __( 'Error: Please specify a %s instance.', 'simplenineteen' ), $platforms[$i]['title'] ); ?>
       </p>
       <button type="button" class="<?php echo $platforms[$i]['slug']; ?> popup-modal__submit"><span><?php esc_attr_e( 'Share', 'simplenineteen' ); ?></span></button>
     </div>
     <?php
   }
 }
 ?>