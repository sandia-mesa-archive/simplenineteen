<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Simple_Nineteen
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	if ( ! simplenineteen_is_frontpage() && ! simplenineteen_is_no_page_title_enabled() ) {
    	echo '<header class="entry-header">';
	    the_title( '<h1 class="entry-title">', '</h1>' );
	    simplenineteen_edit_link( get_the_ID() );
	    echo '</header><!-- .entry-header -->';
	}
	?>
	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . __( 'Pages:', 'simplenineteen' ),
					'after'  => '</div>',
				)
			);
			?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
