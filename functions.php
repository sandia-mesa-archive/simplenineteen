<?php
/**
 * Simple Nineteen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Simple_Nineteen
 * @since 1.0
 */

/**
 * Twenty Seventeen only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function simplenineteen_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyseventeen
	 * If you're building a theme based on Twenty Seventeen, use a find and replace
	 * to change 'simplenineteen' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'simplenineteen' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'simplenineteen-featured-image', 2000, 1200, true );

	add_image_size( 'simplenineteen-thumbnail-avatar', 100, 100, true );

	// Set the default content width.
	$GLOBALS['content_width'] = 525;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus(
		array(
			'top'    => __( 'Top Menu', 'simplenineteen' ),
		)
	);

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support(
		'html5',
		array(
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		)
	);

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support(
		'post-formats',
		array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
			'gallery',
			'audio',
		)
	);

	// Add theme support for Custom Logo.
	add_theme_support(
		'custom-logo',
		array(
			'width'      => 250,
			'height'     => 250,
			'flex-width' => true,
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );


	// Load regular editor styles into the new block-based editor.
	add_theme_support( 'editor-styles' );

	// Load default block styles.
	add_theme_support( 'wp-block-styles' );

	// Add support for responsive embeds.
	add_theme_support( 'responsive-embeds' );

	// Define and register starter content to showcase the theme on new sites.
	$starter_content = array(
		'widgets'     => array(
			// Place three core-defined widgets in the sidebar area.
			'sidebar-1' => array(
				'text_business_info',
				'search',
				'text_about',
			),

			// Add the core-defined business info widget to the footer 1 area.
			'sidebar-2' => array(
				'text_business_info',
			),

			// Put two core-defined widgets in the footer 2 area.
			'sidebar-3' => array(
				'text_about',
				'search',
			),
			'site-info' => array(
				'text_site_info' => array( 'text', array(
					'text' => join( '', array(
					_x( '<p class="sample">Here would be a good spot to put links to your Privacy Policy and Terms of Service.</p>', 'Theme starter content', 'simplenineteen'),
					_x( '<p class="imprint">This website was made using the Simple Nineteen theme.</p>', 'Theme starter content', 'simplenineteen'),
					) ),
				) ),
				'meta',
			),
					
		),

		// Specify the core-defined pages to create and add custom thumbnails to some of them.
		'posts'       => array(
			'home',
			'about'            => array(
				'thumbnail' => '{{image-sandwich}}',
			),
			'contact'          => array(
				'thumbnail' => '{{image-espresso}}',
			),
			'blog'             => array(
				'thumbnail' => '{{image-coffee}}',
			),
			'homepage-section' => array(
				'thumbnail' => '{{image-espresso}}',
			),
		),

		// Create the custom image attachments used as post thumbnails for pages.
		'attachments' => array(
			'image-espresso' => array(
				'post_title' => _x( 'Espresso', 'Theme starter content', 'simplenineteen' ),
				'file'       => 'assets/images/espresso.jpg', // URL relative to the template directory.
			),
			'image-sandwich' => array(
				'post_title' => _x( 'Sandwich', 'Theme starter content', 'simplenineteen' ),
				'file'       => 'assets/images/sandwich.jpg',
			),
			'image-coffee'   => array(
				'post_title' => _x( 'Coffee', 'Theme starter content', 'simplenineteen' ),
				'file'       => 'assets/images/coffee.jpg',
			),
		),

		// Default to a static front page and assign the front and posts pages.
		'options'     => array(
			'show_on_front'  => 'page',
			'page_on_front'  => '{{home}}',
			'page_for_posts' => '{{blog}}',
		),

		// Set up nav menus for each of the two areas registered in the theme.
		'nav_menus'   => array(
			// Assign a menu to the "top" location.
			'top'    => array(
				'name'  => __( 'Top Menu', 'simplenineteen' ),
				'items' => array(
					'link_home', // Note that the core "home" page is actually a link in case a static front page is not used.
					'page_about',
					'page_blog',
					'page_contact',
				),
			),
		),
	);

	/**
	 * Filters Twenty Seventeen array of starter content.
	 *
	 * @since Twenty Seventeen 1.1
	 *
	 * @param array $starter_content Array of starter content.
	 */
	$starter_content = apply_filters( 'simplenineteen_starter_content', $starter_content );

	add_theme_support( 'starter-content', $starter_content );

}
add_action( 'after_setup_theme', 'simplenineteen_setup' );

/**
 * Adds Simple Nineteen theme updater.
 *
 * @since Simple Nineteen 1.0.1
 *
 */

/**
 * Updater class.
 *
 * The theme-review process on w.org takes months.
 * In the meantime this can serve as a simple updater.
 */
class Updater {

	/**
	 * The repository.
	 *
	 * @access private
	 * @since 1.0.1
	 * @var string
	 */
	private $repo;

	/**
	 * Theme name.
	 *
	 * @access private
	 * @since 1.0.1
	 * @var string
	 */
	private $name;

	/**
	 * Theme slug.
	 *
	 * @access private
	 * @since 1.0.1
	 * @var string
	 */
	private $slug;

	/**
	 * Theme URL.
	 *
	 * @access private
	 * @since 1.0.1
	 * @var string
	 */
	private $url;

	/**
	 * The response from the API.
	 *
	 * @access private
	 * @since 1.0.1
	 * @var array
	 */
	private $response;

	/**
	 * Constructor.
	 *
	 * @access public
	 * @since 1.0.1
	 * @param array $args The arguments for this theme.
	 */
	public function __construct( $args ) {
		$this->name = $args['name'];
		$this->slug = $args['slug'];
		$this->repo = $args['repo'];
		$this->ver  = $args['ver'];
		$this->url  = $args['url'];

		$this->response = $this->get_response();
		// Check for theme updates.
		add_filter( 'http_request_args', [ $this, 'update_check' ], 5, 2 );
		// Inject theme updates into the response array.
		add_filter( 'pre_set_site_transient_update_themes', [ $this, 'update_themes' ] );
		add_filter( 'pre_set_transient_update_themes', [ $this, 'update_themes' ] );
	}

	/**
	 * Gets the releases URL.
	 *
	 * @access private
	 * @since 1.0.1
	 * @return string
	 */
	private function get_releases_url() {
		return 'https://code.sandiamesa.com/api/v1/repos/' . $this->repo . '/releases';
	}

	/**
	 * Get the response from the Gitea API.
	 *
	 * @access private
	 * @since 1.0.1
	 * @return array
	 */
	private function get_response() {
		// If ClassicPress or WordPress is installing, skip
		if ( wp_installing() ) {
			return;
		}
		// Remove existing transient
		$cache = get_site_transient( md5( $this->get_releases_url() ) );
		if ( $cache ) {
			delete_site_transient( md5( $this->get_releases_url() ) );
		}
		$response = wp_remote_get( $this->get_releases_url() );
		if ( ! is_wp_error( $response ) && 200 === wp_remote_retrieve_response_code( $response ) ) {
			$response = json_decode( wp_remote_retrieve_body( $response ), true );
			set_site_transient( md5( $this->get_releases_url() ), $response, 12 * HOUR_IN_SECONDS );
		}
		// Return new transient
		return get_site_transient( md5( $this->get_releases_url() ) );

	}

	/**
	 * Get the new version file.
	 *
	 * @access private
	 * @since 1.0.1
	 * @return string
	 */
	private function get_latest_package() {
		if ( ! $this->response ) {
			return;
		}
		foreach ( $this->response as $release ) {
			if ( isset( $release['assets'] ) && isset( $release['assets'][0] ) && isset( $release['assets'][0]['browser_download_url'] ) ) {
				return $release['assets'][0]['browser_download_url'];
			}
		}
	}

	/**
	 * Get the new version.
	 *
	 * @access private
	 * @since 1.0.1
	 * @return string
	 */
	private function get_latest_version() {
		if ( ! $this->response ) {
			return;
		}
		foreach ( $this->response as $release ) {
			if ( isset( $release['tag_name'] ) ) {
				return str_replace( 'v', '', $release['tag_name'] );
			}
		}
	}

	/**
	 * Disables requests to the wp.org repository for this theme.
	 *
	 * @since 1.0.1
	 *
	 * @param array  $request An array of HTTP request arguments.
	 * @param string $url The request URL.
	 * @return array
	 */
	public function update_check( $request, $url ) {
		if ( false !== strpos( $url, '//api.wordpress.org/themes/update-check/1.1/' ) ) {
			$data = json_decode( $request['body']['themes'] );
			unset( $data->themes->{$this->slug} );
			$request['body']['themes'] = wp_json_encode( $data );
		}
		return $request;
	}

	/**
	 * Inject update data for this theme.
	 *
	 * @since 1.0.1
	 *
	 * @param object $transient The pre-saved value of the `update_themes` site transient.
	 * @return object
	 */
	public function update_themes( $transient ) {
		if ( isset( $transient->checked ) ) {
			$current_version = $this->ver;

			if ( version_compare( $current_version, $this->get_latest_version(), '<' ) ) {
				$transient->response[ $this->name ] = [
					'theme'       => $this->name,
					'new_version' => $this->get_latest_version(),
					'url'         => 'https://code.sandiamesa.com/' . $this->repo . '/releases',
					'package'     => $this->get_latest_package(),
				];
			}
		}
		return $transient;
	}
}

new Updater(
  [
   'name' => 'Simple Nineteen', 
   // Theme Name. 
   'repo' => 'sandiamesa/simplenineteen', 
   // Theme repository. 
   'slug' => 'simplenineteen', 
   // Theme Slug. 
   'url' => 'https://code.sandiamesa.com/sandiamesa/simplenineteen', 
   // Theme URL. 
   'ver' => '1.2.1'
   // Theme Version. 
  ] 
);

/**
 * Add the site info section from Twenty Seventeen as a widget.
 *
 * @since 1.3
 */

class Site_Info_widget extends WP_Widget {
	/**
     * Register widget with WordPress/ClassicPress.
     */
    public function __construct() {
        parent::__construct(
            'simplenineteen_site_info_widget', // Base ID
            'Legacy Site Info Widget', // Name
            array( 'description' => __( 'Legacy site info section from Twenty Seventeen as a widget.', 'simplenineteen' ), ) // Args
        );
    }
	
	/**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
	
    public function widget( $args, $instance ) {
        extract( $args );
        $title = apply_filters( 'widget_title', $instance['title'] );
 
        echo $before_widget;
        if ( ! empty( $title ) ) {
            echo $before_title . $title . $after_title;
        }
        if ( function_exists( 'the_privacy_policy_link' ) ) {
                the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
        }
		if ( simplenineteen_is_classicpress() ) { ?>
			<a href="<?php echo esc_url( __( 'https://classicpress.net/', 'simplenineteen' ) ); ?>" class="imprint">
				<?php /* translators: %s: ClassicPress */ printf( __( 'Proudly powered by %s', 'simplenineteen' ), 'ClassicPress' ); ?>
			</a>
		<?php } else { ?>
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'simplenineteen' ) ); ?>" class="imprint">
				<?php /* translators: %s: WordPress */ printf( __( 'Proudly powered by %s', 'simplenineteen' ), 'WordPress' ); ?>
			</a>
		<?php }
        echo $after_widget;
    }
	
	public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php echo esc_html__( 'Title:', 'simplenineteen' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
    <?php
    }
	
	/**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
 
        return $instance;
    }
}

// Register Site_Info_Widget
add_action( 'widgets_init', 'register_site_info' );
     
function register_site_info() { 
    register_widget( 'Site_Info_Widget' ); 
}

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function simplenineteen_content_width() {

	$content_width = $GLOBALS['content_width'];

	// Get layout.
	$page_layout = get_theme_mod( 'page_layout' );

	// Check if layout is one column.
	if ( 'one-column' === $page_layout ) {
		if ( simplenineteen_is_frontpage() ) {
			$content_width = 644;
		} elseif ( is_page() ) {
			$content_width = 740;
		}
	}

	// Check if is single post and there is no sidebar.
	if ( is_single() && ! is_active_sidebar( 'sidebar-1' ) ) {
		$content_width = 740;
	}

	/**
	 * Filter Twenty Seventeen content width of the theme.
	 *
	 * @since Twenty Seventeen 1.0
	 *
	 * @param int $content_width Content width in pixels.
	 */
	$GLOBALS['content_width'] = apply_filters( 'simplenineteen_content_width', $content_width );
}
add_action( 'template_redirect', 'simplenineteen_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function simplenineteen_widgets_init() {
	register_sidebar(
		array(
			'name'          => __( 'Blog Sidebar', 'simplenineteen' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'simplenineteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Footer 1', 'simplenineteen' ),
			'id'            => 'sidebar-2',
			'description'   => __( 'Add widgets here to appear in your footer.', 'simplenineteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Footer 2', 'simplenineteen' ),
			'id'            => 'sidebar-3',
			'description'   => __( 'Add widgets here to appear in your footer.', 'simplenineteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => __( 'Site Info', 'simplenineteen' ),
			'id'            => 'site-info',
			'description'   => __( 'Add widgets here to appear in the Site Info section of your footer. Ideas for this section include a link to your Privacy Policy, a link to your Terms of Service, and a Copyright Notice.', 'simplenineteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'simplenineteen_widgets_init' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function simplenineteen_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf(
		'<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'simplenineteen' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'simplenineteen_excerpt_more' );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Seventeen 1.0
 */
function simplenineteen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'simplenineteen_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function simplenineteen_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'simplenineteen_pingback_header' );

/**
 * Display custom color CSS.
 */
function simplenineteen_colors_css_wrap() {
	if ( 'custom' !== get_theme_mod( 'colorscheme' ) && ! is_customize_preview() ) {
		return;
	}

	require_once( get_parent_theme_file_path( '/inc/color-patterns.php' ) );
	$hue = absint( get_theme_mod( 'colorscheme_hue', 250 ) );

	$customize_preview_data_hue = '';
	if ( is_customize_preview() ) {
		$customize_preview_data_hue = 'data-hue="' . $hue . '"';
	}
	?>
	<style type="text/css" id="custom-theme-colors" <?php echo $customize_preview_data_hue; ?>>
		<?php echo simplenineteen_custom_colors_css(); ?>
	</style>
	<?php
}
add_action( 'wp_head', 'simplenineteen_colors_css_wrap' );

/**
 * Enqueues scripts and styles.
 */
function simplenineteen_scripts() {

	// Theme stylesheet.
	wp_enqueue_style( 'simplenineteen-style', get_stylesheet_uri() );

	// Theme block stylesheet.
	wp_enqueue_style( 'simplenineteen-block-style', get_theme_file_uri( '/assets/css/blocks.css' ), array( 'simplenineteen-style' ), '1.1' );

	// Load the dark colorscheme.
	if ( 'dark' === get_theme_mod( 'colorscheme', 'light' ) || is_customize_preview() ) {
		wp_enqueue_style( 'simplenineteen-colors-dark', get_theme_file_uri( '/assets/css/colors-dark.css' ), array( 'simplenineteen-style' ), '1.0' );
	}
	if ( is_single() && simplenineteen_any_popup_modal_enabled() ) {
		wp_enqueue_style( 'simpleninteen-modal-share', get_theme_file_uri( '/assets/css/modal-share.css' ), array( 'simplenineteen-style' ), '1.1' );
	}

	// Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.
	if ( is_customize_preview() ) {
		wp_enqueue_style( 'simplenineteen-ie9', get_theme_file_uri( '/assets/css/ie9.css' ), array( 'simplenineteen-style' ), '1.0' );
		wp_style_add_data( 'simplenineteen-ie9', 'conditional', 'IE 9' );
	}

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'simplenineteen-ie8', get_theme_file_uri( '/assets/css/ie8.css' ), array( 'simplenineteen-style' ), '1.0' );
	wp_style_add_data( 'simplenineteen-ie8', 'conditional', 'lt IE 9' );

	// Load the html5 shiv.
	wp_enqueue_script( 'html5', get_theme_file_uri( '/assets/js/html5.js' ), array(), '3.7.3' );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'simplenineteen-skip-link-focus-fix', get_theme_file_uri( '/assets/js/skip-link-focus-fix.js' ), array(), '1.0', true );

	if ( is_single() && simplenineteen_any_popup_modal_enabled() ) {
		wp_enqueue_script( 'simplenineteen-modal-share', get_theme_file_uri( '/assets/js/modal-share.js' ), array(), '1.1', true );
	}

	if ( is_single() && simplenineteen_any_not_popup_modal_enabled() ) {
		wp_enqueue_script( 'simplenineteen-nonmodal-share', get_theme_file_uri( '/assets/js/nonmodal-share.js' ), array(), '1.0', true );
	}
	
	$simplenineteen_l10n = array(
		'quote' => simplenineteen_get_svg( array( 'icon' => 'quote-right' ) ),
	);

	if ( has_nav_menu( 'top' ) ) {
		wp_enqueue_script( 'simplenineteen-navigation', get_theme_file_uri( '/assets/js/navigation.js' ), array( 'jquery' ), '1.0', true );
		$simplenineteen_l10n['expand']   = __( 'Expand child menu', 'simplenineteen' );
		$simplenineteen_l10n['collapse'] = __( 'Collapse child menu', 'simplenineteen' );
		$simplenineteen_l10n['icon']     = simplenineteen_get_svg(
			array(
				'icon'     => 'angle-down',
				'fallback' => true,
			)
		);
	}

	wp_enqueue_script( 'simplenineteen-global', get_theme_file_uri( '/assets/js/global.js' ), array( 'jquery' ), '1.1', true );

	wp_enqueue_script( 'jquery-scrollto', get_theme_file_uri( '/assets/js/jquery.scrollTo.js' ), array( 'jquery' ), '2.1.2', true );

	wp_localize_script( 'simplenineteen-skip-link-focus-fix', 'simplenineteenScreenReaderText', $simplenineteen_l10n );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'simplenineteen_scripts' );

/**
 * Enqueues styles for the block-based editor.
 *
 * @since Twenty Seventeen 1.8
 */
function simplenineteen_block_editor_styles() {
	// Block styles.
	wp_enqueue_style( 'simplenineteen-block-editor-style', get_theme_file_uri( '/assets/css/editor-blocks.css' ), array(), '1.1' );
	// Add custom fonts.
}
add_action( 'enqueue_block_editor_assets', 'simplenineteen_block_editor_styles' );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function simplenineteen_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 740 <= $width ) {
		$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
	}

	if ( is_active_sidebar( 'sidebar-1' ) || is_archive() || is_search() || is_home() || is_page() ) {
		if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {
			$sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'simplenineteen_content_image_sizes_attr', 10, 2 );

/**
 * Filter the `sizes` value in the header image markup.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $html   The HTML image tag markup being filtered.
 * @param object $header The custom header object returned by 'get_custom_header()'.
 * @param array  $attr   Array of the attributes for the image tag.
 * @return string The filtered header image HTML.
 */
function simplenineteen_header_image_tag( $html, $header, $attr ) {
	if ( isset( $attr['sizes'] ) ) {
		$html = str_replace( $attr['sizes'], '100vw', $html );
	}
	return $html;
}
add_filter( 'get_header_image_tag', 'simplenineteen_header_image_tag', 10, 3 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array $attr       Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size       Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function simplenineteen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	$attr['sizes'] = '100vw';

	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'simplenineteen_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Use front-page.php when Front page displays is set to a static page.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $template front-page.php.
 *
 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
 */
function simplenineteen_front_page_template( $template ) {
	return is_home() ? '' : $template;
}
add_filter( 'frontpage_template', 'simplenineteen_front_page_template' );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since Twenty Seventeen 1.4
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function simplenineteen_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'simplenineteen_widget_tag_cloud_args' );

/**
 * Get unique ID.
 *
 * This is a PHP implementation of Underscore's uniqueId method. A static variable
 * contains an integer that is incremented with each call. This number is returned
 * with the optional prefix. As such the returned value is not universally unique,
 * but it is unique across the life of the PHP process.
 *
 * @since Simple Nineteen 1.0
 * @see wp_unique_id() Themes requiring WordPress 5.0.3 and greater should use this instead.
 *
 * @staticvar int $id_counter
 *
 * @param string $prefix Prefix for the returned ID.
 * @return string Unique ID.
 */
function simplenineteen_unique_id( $prefix = '' ) {
	static $id_counter = 0;
	if ( function_exists( 'wp_unique_id' ) ) {
		return wp_unique_id( $prefix );
	}
	return $prefix . (string) ++$id_counter;
}

/**
 * Implement the Custom Header feature.
 */
require get_parent_theme_file_path( '/inc/custom-header.php' );

/**
 * Custom template tags for this theme.
 */
require get_parent_theme_file_path( '/inc/template-tags.php' );

/**
 * Additional features to allow styling of the templates.
 */
require get_parent_theme_file_path( '/inc/template-functions.php' );

/**
 * Customizer additions.
 */
require get_parent_theme_file_path( '/inc/customizer.php' );

/**
 * SVG icons functions and filters.
 */
require get_parent_theme_file_path( '/inc/icon-functions.php' );

/**
 * Enable or disable author pages functionality.
 */
function redirect_author_page() {
	 if ( is_author() && 'off' === get_theme_mod( 'author_pages' ) ) {
		 global $wp_query;
		 $wp_query->set_404();
		 status_header( 404 );
		 nocache_headers();
	 }
}
add_action( 'template_redirect', 'redirect_author_page' );

/**
 * 404 error page.
 */
function simplenineteen_404_page() {
	if ( is_404() ) {
		if ( 'custom_page' === get_theme_mod( 'show_on_404' ) ) {
			$custom_page = get_post( get_theme_mod( 'custom_404_page' ) );
			$args = array(
				'post_type' => $custom_page->post_type,
				'page_id' => $custom_page->ID,
				'pagename' => $custom_page->post_title,
			);
			query_posts( $args );
			wp_reset_postdata();
		} else if ( 'redirect' === get_theme_mod( 'show_on_404' ) ) {
			wp_redirect( get_permalink( get_theme_mod( 'custom_404_redirect' ) ) );
		}
	}
}
add_action( 'template_redirect', 'simplenineteen_404_page' );

/**
 * Add custom checkbox to posts editor to enable or disable the display of a featured thumbnail.
 */
function simplenineteen_add_custom_post_meta() {
	add_meta_box('simpleninteen-post-options', esc_html__( 'Simple Nineteen Options', 'simplenineteen'), 'simplenineteen_print_post_meta_box', 'post' );
	add_meta_box('simpleninteen-post-options', esc_html__( 'Simple Nineteen Options', 'simplenineteen'), 'simplenineteen_print_post_meta_box', 'page' );
}
add_action( 'add_meta_boxes', 'simplenineteen_add_custom_post_meta' );

function simplenineteen_print_post_meta_box() {
	wp_nonce_field('simplenineteen-post-meta-box-save', 'simplenineteen-post-meta-box-nonce');
    
	?>
	<p>
		<input type="checkbox" id="simplenineteen-featured-thumbnail-show" name="simplenineteen-feature-thumbnail-show-checkbox" value="1" <?php checked( get_post_meta( get_the_ID(), 'simplenineteen-featured-thumbnail-show', true ) ); ?> />
		<label for="simplenineteen-featured-thumbnail-show"><?php echo esc_html__( 'Show the Featured Thumbnail', 'simplenineteen' ); ?></label>
	</p>
    <?php
    $post_id = $_GET['post'];
    $front_page_id = get_option('page_on_front');
    $posts_page_id = get_option('page_for_posts');

    if ( $post_id != $front_page_id ) {
    ?>
    <p>
		<input type="checkbox" id="simplenineteen-no-page-title" name="simplenineteen-no-page-title-checkbox" value="1" <?php checked( get_post_meta( get_the_ID(), 'simplenineteen-no-page-title', true ) ); ?> />
		<label for="simplenineteen-no-page-title"><?php echo esc_html__( 'Don&#39;t show the page title', 'simplenineteen' ); ?></label>
	</p>
    <?php
    }
    if ( $post_id == $front_page_id ) {
    ?>
    <p>
		<input type="checkbox" id="simplenineteen-big-front-page-header" name="simplenineteen-big-front-page-header-checkbox" value="1" <?php checked( get_post_meta( get_the_ID(), 'simplenineteen-big-front-page-header', true ) ); ?> />
		<label for="simplenineteen-big-front-page-header"><?php echo esc_html__( 'Make the header big', 'simplenineteen' ); ?></label>
	</p>
    <?php
    }
    ?>
    <p id="simplenineteen-fullscreen-option">
		<input type="checkbox" id="simplenineteen-fullscreen" name="simplenineteen-fullscreen-checkbox" value="1" <?php checked( get_post_meta( get_the_ID(), 'simplenineteen-fullscreen', true ) ); ?> />
		<label for="simplenineteen-fullscreen"><?php echo esc_html__( 'Make the page full screen', 'simplenineteen' ); ?></label>
	</p>
    <p id="simplenineteen-show-logo-option" <?php if ( ! get_post_meta(get_the_ID(), 'simplenineteen-fullscreen', true) ) { ?> style="display: none;" <?php } ?> >
        <input type="checkbox" id="simplenineteen-show-logo" name="simplenineteen-logo-checkbox" value="1" <?php checked( get_post_meta( get_the_ID(), 'simplenineteen-show-logo', true ) ); ?> />
		<label for="simplenineteen-show-logo"><?php echo esc_html__( 'Show the site logo', 'simplenineteen' ); ?></label>
	</p>
    <p>
		<input type="checkbox" id="simplenineteen-fullwidth" name="simplenineteen-fullwidth-checkbox" value="1" <?php checked( get_post_meta( get_the_ID(), 'simplenineteen-fullwidth', true ) ); ?> />
		<label for="simplenineteen-fullwidth"><?php echo esc_html__( 'Make the page full width', 'simplenineteen' ); ?></label>
	</p>
	<?php
    $post_type = get_current_screen()->post_type;
    if ( $post_type == 'post' || $post_id == $posts_page_id ) {
    ?>
        <p>
            <input type="checkbox" id="simplenineteen-no-sidebar" name="simplenineteen-no-sidebar-checkbox" value="1" <?php checked( get_post_meta( get_the_ID(), 'simplenineteen-no-sidebar', true ) ); ?> />
		    <label for="simplenineteen-no-sidebar"><?php echo esc_html__( 'Don&#39;t show the sidebar', 'simplenineteen' ); ?></label>
	    </p>
    <?php
    }
}

function simplenineteen_save_custom_post_meta_data( $post_id ) {
	// Get the post type.
	$post_type = get_post_type( $post_id );

	// Check post ID and what the front page's post ID is.
	$front_page_id = get_option('page_on_front');

	// Say no to autosave, kids.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Other security measures.
	if ( ! isset( $_POST['simplenineteen-post-meta-box-nonce'] ) || ! wp_verify_nonce( $_POST['simplenineteen-post-meta-box-nonce'], 'simplenineteen-post-meta-box-save' ) ) {
		return;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}

		// Save featured thumbnail show checkbox setting
	if ( isset( $_POST[ 'simplenineteen-feature-thumbnail-show-checkbox' ] ) ) {
		update_post_meta( $post_id, 'simplenineteen-featured-thumbnail-show', 1 );
	} else {
		delete_post_meta( $post_id, 'simplenineteen-featured-thumbnail-show' );
	}

		// Save fullscreen checkbox setting
		if ( isset( $_POST[ 'simplenineteen-fullscreen-checkbox' ] ) ) {
		update_post_meta( $post_id, 'simplenineteen-fullscreen', 1 );
	} else {
		delete_post_meta( $post_id, 'simplenineteen-fullscreen' );
	}

		// Save fullwidth checkbox setting
		if ( isset( $_POST[ 'simplenineteen-fullwidth-checkbox' ] ) ) {
		update_post_meta( $post_id, 'simplenineteen-fullwidth', 1 );
	} else {
		delete_post_meta( $post_id, 'simplenineteen-fullwidth' );
	}

		// Save show logo checkbox (option is only available when fullscreen is enabled for the post or page)
		if ( isset( $_POST[ 'simplenineteen-fullscreen-checkbox' ] ) && isset( $_POST[ 'simplenineteen-logo-checkbox' ] ) ) {
				update_post_meta( $post_id, 'simplenineteen-show-logo', 1 );
		} else {
				delete_post_meta( $post_id, 'simplenineteen-show-logo' );
		}

		// Save no sidebar checkbox setting
		if ( isset( $_POST[ 'simplenineteen-no-sidebar-checkbox' ] ) && $post_type == 'post' ) {
				update_post_meta( $post_id, 'simplenineteen-no-sidebar', 1 );
		} else {
				delete_post_meta( $post_id, 'simplenineteen-no-sidebar' );
		}

		// Save no page title setting
		if ( isset( $_POST[ 'simplenineteen-no-page-title-checkbox' ] ) && $post_id != $front_page_id ) {
				update_post_meta( $post_id, 'simplenineteen-no-page-title', 1 );
		} else {
				delete_post_meta( $post_id, 'simplenineteen-no-page-title' );
		}

		// Save the big front page header setting
	if ( isset( $_POST['simplenineteen-big-front-page-header-checkbox' ] ) && $post_id == $front_page_id ) {
		update_post_meta( $post_id, 'simplenineteen-big-front-page-header', 1 );
	} else {
		delete_post_meta( $post_id, 'simplenineteen-big-front-page-header' );
	}

}
add_action( 'save_post', 'simplenineteen_save_custom_post_meta_data' );

/**
 * Enqueue the admin edit post/page styles and scripts.
 */
function simplenineteen_enqueue_admin_edit_script( $hook ) {
    if ( 'post.php' == $hook ) {
        wp_enqueue_script( 'simplenineteen-admin-edit', get_theme_file_uri( '/assets/js/admin-edit.js' ), array(), '1.0', true );
        wp_enqueue_style( 'simplenineteen-admin-edit', get_theme_file_uri( '/assets/css/admin-edit.css' ), array(), '1.0' );
    }
}
add_action( 'admin_enqueue_scripts', 'simplenineteen_enqueue_admin_edit_script' );

/**
 * Function to get the list of platforms that Simple Nineteen has share button support for.
 */
function simplenineteen_share_platforms() {
	return array(
			array(
				'title' => 'Mastodon',
				'slug' => 'mastodon',
				'popup' => true,
				'example' => 'https://gameliberty.club',
				'url_before_args' => 'share?text=',
				'hashtags' => true,
				'author' => true,
			),
			array(
				'title' => 'Pleroma',
				'slug' => 'pleroma',
				'popup' => true,
				'example' => 'https://freespeechextremist.com',
				'url_before_args' => '?message=',
				'hashtags' => true,
				'author' => true,
			),
			array(
				'title' => 'GNU Social',
				'slug' => 'gnusocial',
				'popup' => true,
				'example' => 'https://loadaverage.org',
				'url_before_args' => 'notice/new?status_textarea=',
				'hashtags' => true,
				'author' => true,
			),
			array(
				'title' => 'Throat',
				'slug' => 'throat',
				'popup' => true,
				'example' => 'https://poal.co',
				'url_before_args' => 'submit/link?title=',
				'before_url_param' => '&url=',
				'hashtags' => false,
				'author' => false,
			),
			array(
				'title' => 'Email',
				'slug' => 'email',
				'popup' => false,
				'url_before_args' => 'mailto:?subject=',
				'hashtags' => false,
				'author' => false,
				'title_in_link' => true,
				'before_url_param' => '&body=',
			),
			array(
				'title' => 'Telegram',
				'slug' => 'telegram',
				'popup' => false,
				'url_before_args' => 'https://telegram.me/share/url?text=',
				'hashtags' => false,
				'author' => false,
				'title_in_link' => true,
				'before_url_param' => '&url=',
			),
			array(
				'title' => 'MeWe',
				'slug' => 'mewe',
				'popup' => false,
				'url_before_args' => 'https://mewe.com/share?link=',
				'hashtags' => false,
				'author' => false,
				'title_in_link' => false,
			),
			array(
				'title' => 'Pocket',
				'slug' => 'pocket',
				'popup' => false,
				'url_before_args' => 'https://getpocket.com/save?title=',
				'hashtags' => false,
				'author' => false,
				'title_in_link' => true,
				'before_url_param' => '&url=',
			),
			array(
				'title' => 'Twitter',
				'slug' => 'twtr',
				'popup' => false,
				'url_before_args' => 'https://twitter.com/intent/tweet?text=',
				'hashtags' => true,
				'author' => true,
				'title_in_link' => true,
				'author_placeholder' => '@SandiaMesa',
			),
			array(
				'title' => 'Reddit',
				'slug' => 'reddit',
				'popup' => false,
				'url_before_args' => 'https://www.reddit.com/submit?title=',
				'hashtags' => false,
				'author' => false,
				'title_in_link' => true,
				'before_url_param' => '&url=',
			),
			array(
				'title' => 'VK',
				'slug' => 'vk',
				'popup' => false,
				'url_before_args' => 'https://vk.com/share.php?url=',
				'hashtags' => false,
				'author' => false,
				'title_in_link' => false,
			),
			array(
				'title' => 'Facebook',
				'slug' => 'fb',
				'popup' => false,
				'url_before_args' => 'https://facebook.com/sharer/sharer.php?u=',
				'hashtags' => false,
				'author' => false,
				'title_in_link' => false,
			),
	);
}

/**
 * Confirm password.
 */

function wpum_my_psw_field_confirmation( $fields ) {

	$fields[ 'confirm_psw' ] = array(
		'label'       => 'Confirm password',
		'type'        => 'password',
		'meta'        => false,
		'required'    => true,
		'priority' => 30
	);

	return $fields;

}
add_filter( 'wpum_get_registration_fields', 'wpum_my_psw_field_confirmation' );

function wpum_verify_my_psw_confirmation( $pass, $fields, $values, $form ) {

	if ( $form === 'registration' && isset( $values['register']['confirm_psw'] ) ) {

		$psw1 = $values['register']['user_password'];
		$psw2 = $values['register']['confirm_psw'];

		if ( $psw1 !== $psw2 ) {
			return new WP_Error( 'psw-validation-error', 'Passwords do not match.' );
		}

	}

	return $pass;

}
add_filter( 'submit_wpum_form_validate_fields', 'wpum_verify_my_psw_confirmation', 10, 4 );

// Function to handle the thumbnail request
function get_the_post_thumbnail_src($img)
{
  return (preg_match('~\bsrc="([^"]++)"~', $img, $matches)) ? $matches[1] : '';
}

//This is the consent checkbox for Privacy
add_filter(
	'wpum_privacy_text',
	function( $text ) {

		return 'I give Sandia Mesa Animation Studios my consent to use this information for the purposes specifically outlined in the <a href="/privacy-policy">Privacy Policy</a>.';

	}
);

//This is the terms of service checbox
add_filter(
	'wpum_terms_text',
	function( $text ) {

		return 'I agree to the <a href="/tos">Terms of Service</a>.';

	}
);
//Remove WP Emoji canvas fingerprinting code bloat
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');
add_filter('emoji_svg_url', '__return_false');