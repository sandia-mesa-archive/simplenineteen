# Authors
Simple Nineteen is available on Sandia Mesa's self-hosted [Gitea](https://code.sandiamesa.com/sandiamesa/simplenineteen) and is provided thanks to the work of the following contributors:

* Sean King - <sean.king@sandiamesa.com>

Additionally, Sandia Mesa is grateful for the work done by the [WordPress](https://wordpress.org/themes/twentyseventeen/) team to make Twenty Seventeen such a great theme. Their accomplishments have inspired us to embrace, use, and publish free and open-source software as part of our business model.

