<?php
/**
 * Simple Nineteen: Customizer
 *
 * @package Simple_Nineteen
 * @since 1.0
 */

/**
 * Customizer: Add alpha option for gradient color.
 * Source: https://gist.github.com/stevenmunro/3dd2d76ab25667b40029
 */
function pluto_add_customizer_custom_controls( $wp_customize ) {

    class Pluto_Customize_Alpha_Color_Control extends WP_Customize_Control {
    
        public $type = 'alphacolor';
        //public $palette = '#3FADD7,#555555,#666666, #F5f5f5,#333333,#404040,#2B4267';
        public $palette = true;
    
        protected function render() {
            $id = 'customize-control-' . str_replace( '[', '-', str_replace( ']', '', $this->id ) );
            $class = 'customize-control customize-control-' . $this->type; ?>
            <li id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $class ); ?>">
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
                <?php $this->render_content(); ?>
            </li>
        <?php }
    
        public function render_content() { ?>
                <input type="text" data-palette="<?php echo $this->palette; ?>" data-default-color="<?php echo $this->setting->default; ?>" value="<?php echo intval( $this->value() ); ?>" class="pluto-color-control" <?php $this->link(); ?> />
        <?php }
    }

}
add_action( 'customize_register', 'pluto_add_customizer_custom_controls' );

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */

function simplenineteen_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	$wp_customize->selective_refresh->add_partial(
		'blogname',
		array(
			'selector'        => '.site-title a',
			'render_callback' => 'simplenineteen_customize_partial_blogname',
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'blogdescription',
		array(
			'selector'        => '.site-description',
			'render_callback' => 'simplenineteen_customize_partial_blogdescription',
		)
	);

	/**
	 * Custom colors.
	 */
	$wp_customize->add_setting(
		'colorscheme',
		array(
			'default'           => 'light',
			'transport'         => 'postMessage',
			'sanitize_callback' => 'simplenineteen_sanitize_colorscheme',
		)
	);

	$wp_customize->add_setting(
		'colorscheme_hue',
		array(
			'default'           => 250,
			'transport'         => 'postMessage',
			'sanitize_callback' => 'absint', // The hue is stored as a positive integer.
		)
	);

	$wp_customize->add_control(
		'colorscheme',
		array(
			'type'     => 'radio',
			'label'    => __( 'Color Scheme', 'simplenineteen' ),
			'choices'  => array(
				'light'  => __( 'Light', 'simplenineteen' ),
				'dark'   => __( 'Dark', 'simplenineteen' ),
				'custom' => __( 'Custom', 'simplenineteen' ),
			),
			'section'  => 'colors',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'colorscheme_hue',
			array(
				'mode'     => 'hue',
				'section'  => 'colors',
				'priority' => 6,
			)
		)
	);

	/**
	 * Theme options.
	 */
	$wp_customize->add_section(
		'theme_options',
		array(
			'title'    => __( 'Theme Options', 'simplenineteen' ),
			'priority' => 130, // Before Additional CSS.
		)
	);

	$wp_customize->add_setting(
		'page_layout',
		array(
			'default'           => 'two-column',
			'sanitize_callback' => 'simplenineteen_sanitize_page_layout',
			'transport'         => 'postMessage',
		)
	);

	$wp_customize->add_control(
		'page_layout',
		array(
			'label'           => __( 'Page Layout', 'simplenineteen' ),
			'section'         => 'theme_options',
			'type'            => 'radio',
			'description'     => __( 'When the two-column layout is assigned, the page title is in one column and content is in the other.', 'simplenineteen' ),
			'choices'         => array(
				'one-column' => __( 'One Column', 'simplenineteen' ),
				'two-column' => __( 'Two Column', 'simplenineteen' ),
			),
			'active_callback' => 'simplenineteen_is_view_with_layout_option',
		)
	);
	
	$wp_customize->add_setting( 'header_background_color', array(
		'default'           => '#ffffff',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport'         => 'postMessage',
		)
	);
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_background_color', array(
		'label'       => __( 'Header Background Color', 'simplenineteen' ),
		'description' => __( 'This is the color of the background that appears when no header image exists.', 'simplenineteen' ),
		'section'     => 'colors',
	) ) );
	
	/**
	  Enable or disable the fancy gradient on the header.
	**/
	$wp_customize->add_setting(
		'grey_gradient_header',
		array(
			'default'           => 'on',
			'sanitize_callback' => 'simplenineteen_sanitize_grey_gradient_header',
			'transport'         => 'postMessage',
		)
	);

	$wp_customize->add_control(
		'grey_gradient_header',
		array(
			'label'           => __( 'Header Gradient', 'simplenineteen' ),
			'section'         => 'header_image',
			'type'            => 'radio',
			'description'     => __( 'Choose whether you want to show a gradient in the header.', 'simplenineteen' ),
			'choices'         => array(
				'on' => __( 'On', 'simplenineteen' ),
				'off' => __( 'Off', 'simplenineteen' ),
			),
		)
	);
	
	/**
	 * Color of the other end of the gradient.
	 */
	$wp_customize->add_setting(
    	'gradient_color',
		array(
			'default'           => 'rgba(0,0,0,0.3)',
			'sanitize_callback'	=> 'simplenineteen_sanitize_rgba_color',
			'transport'         => 'postMessage',
		)
	);

	$wp_customize->add_control(
    	new Pluto_Customize_Alpha_Color_Control(
        	$wp_customize,
        	'gradient_color',
        	array(
            	'label'       => __( 'Header Gradient Color', 'simplenineteen' ),
				'description' => __( 'This is the color of the bottom end of the header gradient.', 'simplenineteen' ),
				'section'     => 'colors',
            	'palette' => true,
        	)
    	)
	);
  
    /**
	  Enable or disable author pages.
	**/
	$wp_customize->add_setting(
		'author_pages',
		array(
			'default'           => 'on',
			'sanitize_callback' => 'simplenineteen_sanitize_author_pages',
			'transport'         => 'postMessage',
		)
	);

	$wp_customize->add_control(
		'author_pages',
		array(
			'label'           => __( 'Author Pages', 'simplenineteen' ),
			'section'         => 'theme_options',
			'type'            => 'radio',
			'description'     => __( 'Choose whether you want to enable or disable author pages.', 'simplenineteen' ),
			'choices'         => array(
				'on' => __( 'On', 'simplenineteen' ),
				'off' => __( 'Off', 'simplenineteen' ),
			),
		)
	);
  
    /**
     * Choose what will be displayed upon a 404 error.
     */
    $wp_customize->add_setting( 'show_on_404', array(
		'default' => 'default_page',
        'sanitize_callback' => 'simplenineteen_sanitize_show_on_404',
		'transport' => 'postMessage',
	) );

	$wp_customize->add_control( 'show_on_404', array(
		'label' => __( 'A 404 error', 'simplenineteen' ),
		'section' => '404_error_options',
		'type' => 'radio',
		'choices' => array(
			'default_page'    =>  __( 'Displays Simple Nineteen&#8217;s default 404 page', 'simplenineteen' ),
            'custom_page'   =>  __( 'Displays a custom page', 'simplenineteen' ),
            'redirect'     =>  __( 'Redirects to another page', 'simplenineteen' ),
		),
	) );
    
    /**
     * Custom 404 page.
     */
    $wp_customize->add_setting( 'custom_404_page', array(
        'default' => false,
        'sanitize_callback' => 'absint',
        'transport' => 'postMessage',
    ) );
    
    $wp_customize->add_control( 'custom_404_page', array(
        'label' => __( 'Custom 404 Error Page:', 'simplenineteen' ),
        'section' => '404_error_options',
        'type' => 'dropdown-pages',
        'allow_addition' => true,
    ) );
    
    /**
     * Custom 404 redirect.
     */
    $wp_customize->add_setting( 'custom_404_redirect', array(
        'default' => false,
        'sanitize_callback' => 'absint',
        'transport' => 'postMessage',
    ) );
    
    $wp_customize->add_control( 'custom_404_redirect', array(
        'label' => __( 'Custom 404 Error Redirect:', 'simplenineteen' ),
        'section' => '404_error_options',
        'type' => 'dropdown-pages',
        'allow_addition' => true,
    ) );
    /**
     * Share buttons section
     */
    $wp_customize->add_section(
      'share_button_settings',
      array(
        'title'    => __( 'Share Button Settings', 'simplenineteen' ),
        'priority' => 127, // Before Theme Options
        'description' => __( 'You can choose which share buttons you want to show on posts here.', 'simplenineteen' ),
      )
    );

    /**
     * Share button hashtags
     */
    $wp_customize->add_setting( 'share_hashtags', array(
       'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control( 'share_hashtags', array(
       'label' => __( 'Hashtags', 'simplenineteen' ),
       'description' => __( 'Default hashtags set for each post when supported by the share button. Separate each with a , comma.', 'simplenineteen' ),
       'section' => 'share_button_settings',
       'type' => 'text',
    ));

    /**
     * Share button username for fediverse
     */
    $wp_customize->add_setting( 'share_fediverse_author', array(
       'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control( 'share_fediverse_author', array(
       'label' => __( 'Fediverse username', 'simplenineteen' ),
       'section' => 'share_button_settings',
       'type' => 'text',
       'input_attrs' => array(
            'placeholder' => __( '@SandiaMesa@freespeechextremist.com', 'simplenineteen' ),
        ),
    ));
    /**
     * Share buttons options.
     */
    $platforms = simplenineteen_share_platforms();
    for ( $i = 0; $i < count( $platforms ); $i++ ) {
      $wp_customize->add_setting( 'share_button_' . $platforms[$i]['slug'], array(
        'default'    => '1',
      ));
      $wp_customize->add_control( 'share_button_' . $platforms[$i]['slug'], array(
        'type' => 'checkbox',
        'label' => __( $platforms[$i]['title'], 'simplenineteen' ),
        'section' => 'share_button_settings',
      ));
      if ( $platforms[$i]['author'] == true && $platforms[$i]['popup'] == false ) {
        $wp_customize->add_setting( 'share_button_' . $platforms[$i]['slug'] . '_author', array(
          'sanitize_callback' => 'sanitize_text_field',
        ));
        $wp_customize->add_control( 'share_button_' . $platforms[$i]['slug'] . '_author', array(
          'label' => __( $platforms[$i]['title'] . ' username', 'simplenineteen' ),
          'section' => 'share_button_settings',
          'type' => 'text',
          'input_attrs' => array(
               'placeholder' => __( $platforms[$i]['author_placeholder'], 'simplenineteen' ),
           ),
        ));
      }
    }
}
add_action( 'customize_register', 'simplenineteen_customize_register' );

/**
 * Sanitize the page layout options.
 *
 * @param string $input Page layout.
 */
function simplenineteen_sanitize_page_layout( $input ) {
	$valid = array(
		'one-column' => __( 'One Column', 'simplenineteen' ),
		'two-column' => __( 'Two Column', 'simplenineteen' ),
	);

	if ( array_key_exists( $input, $valid ) ) {
		return $input;
	}

	return '';
}

/**
 * Sanitize the option for what to display upon a 404 error.
 *
 * @param string $input A 404 error displays.
 */
function simplenineteen_sanitize_show_on_404( $input ) {
	$valid = array(
		'default_page'    =>  __( 'Displays Simple Nineteen&#8217;s default 404 page', 'simplenineteen' ),
        'custom_page'   =>  __( 'Displays a custom page', 'simplenineteen' ),
        'redirect'     =>  __( 'Redirects to another page', 'simplenineteen' ),
	);

	if ( array_key_exists( $input, $valid ) ) {
		return $input;
	}

	return '';
}

/**
 * Sanitize the grey gradient option.
 *
 * @param string $input Grey Gradient.
 */
function simplenineteen_sanitize_grey_gradient_header( $input ) {
	$valid = array(
		'on' => __( 'On', 'simplenineteen' ),
		'off' => __( 'Off', 'simplenineteen' ),
	);

	if ( array_key_exists( $input, $valid ) ) {
		return $input;
	}

	return '';
}

/**
 * Sanitize RGBA color.
 * (Based off ClassicPress core's sanitize_hex_color function)
 *
 * @param string $color
 * @return string|void
 */
function simplenineteen_sanitize_rgba_color( $input ) {
	
	if ( '' === $input ) {
		return '';
	}

	// 3 or 6 hex digits, or rgba or hsla, or the empty string.
	if ( preg_match('/(#([0-9a-f]{3}){1,2}|(rgba|hsla)\(\d{1,3}%?(,\s?\d{1,3}%?){2},\s?(1|0|0?\.\d+)\))/i', $input ) ) {
		return $input;
	}
}

/**
 * Sanitize the author pages option.
 *
 * @param string $input Author Pages.
 */
function simplenineteen_sanitize_author_pages( $input ) {
	$valid = array(
		'on' => __( 'On', 'simplenineteen' ),
		'off' => __( 'Off', 'simplenineteen' ),
	);

	if ( array_key_exists( $input, $valid ) ) {
		return $input;
	}

	return '';
}

/**
 * Sanitize the colorscheme.
 *
 * @param string $input Color scheme.
 */
function simplenineteen_sanitize_colorscheme( $input ) {
	$valid = array( 'light', 'dark', 'custom' );

	if ( in_array( $input, $valid, true ) ) {
		return $input;
	}

	return 'light';
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @since Twenty Seventeen 1.0
 * @see simplenineteen_customize_register()
 *
 * @return void
 */
function simplenineteen_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @since Twenty Seventeen 1.0
 * @see simplenineteen_customize_register()
 *
 * @return void
 */
function simplenineteen_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Return whether we're previewing the front page and it's a static page.
 */
function simplenineteen_is_static_front_page() {
	return ( is_front_page() && ! is_home() );
}
/**
 * Return whether we're on a view that supports a one or two column layout.
 */
function simplenineteen_is_view_with_layout_option() {
	// This option is available on all pages. It's also available on archives when there isn't a sidebar.
	return ( is_page() || ( is_archive() && ! is_active_sidebar( 'sidebar-1' ) ) );
}

/**
 * Bind JS handlers to instantly live-preview changes.
 */
function simplenineteen_customize_preview_js() {
	wp_enqueue_script( 'simplenineteen-customize-preview', get_theme_file_uri( '/assets/js/customize-preview.js' ), array( 'customize-preview' ), '1.1', true );
}
add_action( 'customize_preview_init', 'simplenineteen_customize_preview_js' );

/**
 * Load dynamic logic for the customizer controls area.
 */
function simplenineteen_panels_js() {
	wp_enqueue_script( 'simplenineteen-customize-controls', get_theme_file_uri( '/assets/js/customize-controls.js' ), array(), '1.2', true );
}
add_action( 'customize_controls_enqueue_scripts', 'simplenineteen_panels_js' );

/**
 * Load alpha controller scripts and styles.
 * Source: https://gist.github.com/stevenmunro/3dd2d76ab25667b40029
 */
function pluto_enqueue_customizer_admin_scripts() {
	wp_enqueue_script( 'customizer-admin-js', get_theme_file_uri( '/assets/js/alpha-controller.js' ), array( 'jquery' ), '1.3', true );
	wp_enqueue_style( 'pluto-customizer-controls', get_theme_file_uri( '/assets/css/alpha-controller.css' ), NULL, '1.3', 'all' );
}
add_action( 'admin_enqueue_scripts', 'pluto_enqueue_customizer_admin_scripts' );