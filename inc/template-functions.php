<?php
/**
 * Additional features to allow styling of the templates
 *
 * @package Simple_Nineteen
 * @since 1.0
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function simplenineteen_body_classes( $classes ) {
	// Add class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Add class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Add class if we're viewing the Customizer for easier styling of theme options.
	if ( is_customize_preview() ) {
		$classes[] = 'simplenineteen-customizer';
	}

	// Add class on front page.
	if ( is_front_page() && 'posts' !== get_option( 'show_on_front' ) ) {
		$classes[] = 'simplenineteen-front-page';
	}
	
	// Add class on the custom 404 error page.
	if ( simplenineteen_is_custom_404() ) {
		$classes[] = 'error404';
	}

    // Add a class if the page is full width.
    if ( simplenineteen_is_fullwidth() ) {
        $classes[] = 'simplenineteen-fullwidth';
    }
	
	// Add a class if the page is full screen.
	if ( simplenineteen_is_fullscreen() ) {
		$classes[] = 'simplenineteen-fullscreen';
	}

	// Add a class if there is a custom header.
	if ( has_header_image() ) {
		$classes[] = 'has-header-image';
	}

	// Add class if sidebar is used.
	if ( is_active_sidebar( 'sidebar-1' ) && !is_page() && !simplenineteen_is_no_sidebar_enabled() ) {
		$classes[] = 'has-sidebar';
	}

	// Add class for one or two column page layouts.
	if ( is_page() || is_archive() ) {
		if ( 'one-column' === get_theme_mod( 'page_layout' ) ) {
			$classes[] = 'page-one-column';
		} else {
			$classes[] = 'page-two-column';
		}
	}
	// Add class if the site title and tagline is hidden.
	if ( 'blank' === get_header_textcolor() ) {
		$classes[] = 'title-tagline-hidden';
	}

	// Get the colorscheme or the default if there isn't one.
	$colors    = simplenineteen_sanitize_colorscheme( get_theme_mod( 'colorscheme', 'light' ) );
	$classes[] = 'colors-' . $colors;

	return $classes;
}
add_filter( 'body_class', 'simplenineteen_body_classes' );

/**
 * Checks to see if we're on the front page or not.
 */
function simplenineteen_is_frontpage() {
	return ( is_front_page() && ! is_home() );
}

/**
 * Checks to see if the page or post is set to be fullscreen.
 */
function simplenineteen_is_fullscreen() {
    return (get_post_meta( get_queried_object_id(), 'simplenineteen-fullscreen', true ) );
}

/**
 * Checks to see if the page or post is set to be full width.
 */
function simplenineteen_is_fullwidth() {
    return (get_post_meta( get_queried_object_id(), 'simplenineteen-fullwidth', true ) );
}

/**
 * Check to see if the post is set to have no sidebar.
 */
function simplenineteen_is_no_sidebar_enabled() {
    return (get_post_meta( get_queried_object_id(), 'simplenineteen-no-sidebar', true ) );
}

/**
 * Check to see if the front page has the big header enabled.
 */
function simplenineteen_is_big_front_page_header_enabled() {
	return (get_post_meta( get_queried_object_id(), 'simplenineteen-big-front-page-header', true ) );
}

/**
 * Check to see if we're using ClassicPress or not.
 */
function simplenineteen_is_classicpress() {
	return ( function_exists( 'classicpress_version' ) );
}

/**
 * Check to see if the page or post is set to have no page title.
 */
function simplenineteen_is_no_page_title_enabled() {
    return (get_post_meta( get_queried_object_id(), 'simplenineteen-no-page-title', true ) );
}

/**
 * Check to see if the fullscreen page has the logo included.
 */
function simplenineteen_is_logo_included() {
    return (get_post_meta( get_queried_object_id(), 'simplenineteen-show-logo', true ) );
}

/**
 * Check to see if we're on the custom 404 error page.
 */
function simplenineteen_is_custom_404() {
	return (get_queried_object_id() == get_theme_mod( 'custom_404_page' ) && 'custom_page' == get_theme_mod( 'show_on_404' ));
}

/**
 * Convert hex and rgba colors to be usable for our gradient.
 *
 * Sources used to help build this function:
 * - https://stackoverflow.com/questions/15202079/convert-hex-color-to-rgb-values-in-php
 * - https://developer.wordpress.org/reference/functions/sanitize_hex_color/
 * - https://wordpress.stackexchange.com/questions/257581/escape-hexadecimals-rgba-values#262578
 * - https://stackoverflow.com/questions/3451906/multiple-returns-from-a-function#3579950
 */
function simplenineteen_get_color( $option ) {
	$color = get_theme_mod( $option );
	if ( preg_match('|^#([A-Fa-f0-9]{3}){1,2}$|', $color) ) {
		$hex = str_replace('#', '', $color);
		$split = str_split( $hex, 2);
		$vala = hexdec($split[0]);
		$valb = hexdec($split[1]);
		$valc = hexdec($split[2]);
		$alpha = 1;
		$type = 'rgba';
	} else if ( strpos( $color, 'rgba' ) !== false ) {
		sscanf( $color, 'rgba(%d,%d,%d,%f)', $vala, $valb, $valc, $alpha );
		$type = 'rgba';
	} else {
		sscanf( $color, 'hsla(%d,%d,%d,%f)', $vala, $valb, $valc, $alpha );
		$type = 'hsla';
	}

	return array( 'type' => $type, 'vala' => $vala, 'valb' => $valb, 'valc' => $valc, 'alpha' => $alpha, 'color' => $color);
}

/**
 * Get share button option
 */
function simplenineteen_get_share_option( $platform ) {
	return get_theme_mod ( 'share_button_' . $platform, '1' );
}

/**
 * Check to see if any popup-based social modals are enabled.
 */
function simplenineteen_any_popup_modal_enabled() {
	$platforms = simplenineteen_share_platforms();

	for ( $i = 0; $i < count($platforms); $i++ ) {
		if ($platforms[$i]['popup'] == true && get_theme_mod( 'share_button_' . $platforms[$i]['slug'], '1') == '1') {
			return true;
		}
	}

	return false;
}

/**
 * Check to see if any not popup-based social modals are enabled.
 */
function simplenineteen_any_not_popup_modal_enabled() {
	$platforms = simplenineteen_share_platforms();

	for ( $i = 0; $i < count($platforms); $i++ ) {
		if ($platforms[$i]['popup'] == false && get_theme_mod( 'share_button_' . $platforms[$i]['slug'], '1') == '1') {
			return true;
		}
	}

	return false;
}

/**
 * Check to see if any share buttons are enabled.
 */
function simplenineteen_any_share_button_enabled() {
	$platforms = simplenineteen_share_platforms();

	for ( $i = 0; $i < count($platforms); $i++ ) {
		if (get_theme_mod( 'share_button_' . $platforms[$i]['slug'], '1') == '1') {
			return true;
		}
	}

	return false;
}

/**
 * Check to see if any custom share meta options are set.
 */
function simplenineteen_any_custom_share_meta_options_set() {
	$platforms = simplenineteen_share_platforms();
	$hashtags = get_theme_mod('share_hashtags');

	if ( isset( $hashtags ) && $hashtags != '' ) {
		return true;
	}

	$fediverseAuthor = get_theme_mod( 'share_fediverse_author' );

	if ( isset( $fediverseAuthor ) && $fediverseAuthor != '' ) {
		return true;
	}

	$platforms_set_author_count = 0;

	for ( $i = 0; $i < count($platforms); $i++) {
		$platformSetting = get_theme_mod( 'share_button_' . $platforms[$i]['slug'] );
		$platformUsernameSetting = get_theme_mod( 'share_button_' . $platforms[$i]['slug'] . '_author' );
		if ( isset($platformUsernameSetting) && $platformUsernameSetting != '' && $platformSetting == '1' ) {
			return true;
		}
	}

	return false;
}
