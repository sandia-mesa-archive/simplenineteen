# Simple Nineteen

**Contributors:** sandiamesa<br>
**Requires at least:** WordPress 4.7<br>
**Tested up to:** WordPress 5.0<br>
**Requires PHP:** 5.2.4<br>
**Version:** 1.2.1<br>
**License:** GPLv2 or later<br>
**License URI:** https://www.gnu.org/licenses/gpl-2.0.html<br>
**Tags:** one-column, two-columns, right-sidebar, flexible-header, accessibility-ready, custom-colors, custom-header, custom-menu, custom-logo, editor-style, featured-images, footer-widgets, post-formats, rtl-language-support, sticky-post, theme-options, threaded-comments, translation-ready, full-width, full-screen, social-share

## Description
Simple Nineteen is the theme used by Sandia Mesa Animation Studios to power many of its websites. Forked from the WordPress Twenty Seventeen theme, Simple Nineteen has a focus on business sites with its features including widgets and navigation menus, a logo, social media share buttons for various platforms like Twitter, Facebook, Reddit, Mastodon, and Pleroma, and more. You can personalize its asymmetrical grid using a custom color scheme and showcase your multimedia content with post formats. Additionally, you can change the header's background color and remove the grey gradient included in the theme. Options for full width and full screen pages are also included without the need for an additional plugin. This theme works great in many languages, for any abilities, and on any device.

For more information about Simple Nineteen, please go to https://code.sandiamesa.com/sandiamesa/simplenineteen/

## Installation

1. Go to https://code.sandiamesa.com/sandiamesa/simplenineteen/releases and download 'simplenineteen.(version).zip'.
2. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
3. Click the 'Upload Theme' button. Then, click 'Browse' and open the zip file you downloaded.
4. Click 'Install now'. Then, activate the theme.
5. Go to https://code.sandiamesa.com/sandiamesa/simplenineteen/wiki for a guide on how to customize this theme.
6. Navigate to Appearance > Customize in your admin panel and customize to taste.

## Copyright

Copyright (C) 2019-2020 Sandia Mesa Animation Studios and other contributors (see [AUTHORS.md](AUTHORS.md))<br>
Copyright (C) 2016 WordPress.org<br>
Simple Nineteen is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

Simple Nineteen bundles the following third-party resources:

HTML5 Shiv, Copyright 2014 Alexander Farkas<br>
Licenses: MIT/GPL2<br>
Source: https://github.com/aFarkas/html5shiv

jQuery scrollTo, Copyright 2007-2015 Ariel Flesler<br>
License: MIT<br>
Source: https://github.com/flesler/jquery.scrollTo

normalize.css, Copyright 2012-2018 Nicolas Gallagher and Jonathan Neal<br>
License: MIT<br>
Source: https://necolas.github.io/normalize.css/

Font Awesome icons, Copyright Fonticons, Inc.<br>
License: Creative Commons Attribution 4.0 International (CC BY 4.0)<br>
Source: https://fontawesome.com

Bundled header image, Copyright Alvin Engler<br>
License: CC0 1.0 Universal (CC0 1.0)<br>
Source: https://unsplash.com/@englr?photo=bIhpiQA009k

Libre Franklin, Copyright 2015 Impallari Type<br>
License: SIL Open Font License, version 1.1.<br>
Source: https://github.com/impallari/Libre-Franklin

Gitea Theme Updater, Copyright 2019-2020 Sandia Mesa Animation Studios and Aristeides Stathopoulos<br>
License: MIT<br>
Source: https://code.sandiamesa.com/sandiamesa/gitea-theme-updater

## Changelog

### [Unreleased]
* Release date: TBA

Added:
* Old Site Info section from Twenty Seventeen as a widget. Can detect if the site's using ClassicPress and change "Proudly powered by WordPress" to "Proudly powered by ClassicPress" when that's the case.
* Ability to change the 404 error page to either display the default one provided by Simple Nineteen, display a custom page you made, or redirect to another page.
* Ability to enable or disable share buttons
* Redone share button system

Fixed:
* Styling of share modals (especially on mobile devices)
* Rendering of header gradients on iOS devices
* Starter content error caused by custom post meta.
* Notice that appeared when using the get_current_screen() function for rendering the post meta boxes.

### 1.2.1
* Released: May 8, 2020

Fixed:
* Issue with header gradient not rendering.

### 1.2
* Released: May 7, 2020

Added:
* New options section for each page and post
* Ability to enable or disable the showing of the featured thumbnail
* Ability to disable showing the page or post's title.
* Ability to change the color of the bottom end of the header's gradient when enabled.
* Ability to enable or disable the big header on the front page

Changed:
* Full screen, include site logo, full width, and no sidebar customization methods have been changed to be part of the new options section.
* Templates such as Full Screen, Full Width, and Full Screen (w/Branding) are now deprecated.
* Upgraded normalize.css from v5.0.0 to v8.0.1

### 1.1.1
* Released: April 9, 2020

Fixed:
* Gitea theme updater conflicting with WordPress OpenID plugin

### 1.1
* Released: January 5, 2020

Added:
* New author pages
* Customizer option to enable or disable author pages

Updated:
* Font Awesome 4.7 icons to Font Awesome 5 icons

### 1.0.3
* Released: December 30, 2019

Only changed theme versions for testing updates.

### 1.0.2
* Released: December 30, 2019

Only changed theme versions for testing updates.

### 1.0.1
* Released: December 30, 2019

Added:
* Theme updater

Fixed:
* Search page error

### 1.0
* Released: December 6, 2019

Initial release