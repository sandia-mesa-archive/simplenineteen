# Contributing
Thank you for considering contributing to the Simple Nineteen theme.

You can contribute in the following ways.

* Finding and reporting bugs
* Translating the Simple Nineteen theme's interface into various languages
* Contributing code to Simple Nineteen by fixing bugs or implementing features
* Improving the documentation

## Bug Reports and Feature Requests
Bug reports and feature suggestions can be submitted to the [Gitea Issues](https://code.sandiamesa.com/sandiamesa/simplenineteen/issues) tracker for Simple Nineteen. Please make sure you're not submitting duplicates, and that a similar request or report has not already been resolved or rejected in the past using the search function. Also, please use clear and concise titles.