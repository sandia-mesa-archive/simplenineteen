/**
 * Scripts within the customizer controls window.
 *
 * Contextually shows the color hue control and informs the preview
 * when users open or close the front page sections section.
 */

(function() {
	wp.customize.bind( 'ready', function() {

		// Only show the color hue control when there's a custom color scheme.
		wp.customize( 'colorscheme', function( setting ) {
			wp.customize.control( 'colorscheme_hue', function( control ) {
				var visibility = function() {
					if ( 'custom' === setting.get() ) {
						control.container.slideDown( 180 );
					} else {
						control.container.slideUp( 180 );
					}
				};

				visibility();
				setting.bind( visibility );
			});
		});

		// Detect when the front page sections section is expanded (or closed) so we can adjust the preview accordingly.
		wp.customize.section( 'theme_options', function( section ) {
			section.expanded.bind( function( isExpanding ) {

				// Value of isExpanding will = true if you're entering the section, false if you're leaving it.
				wp.customize.previewer.send( 'section-highlight', { expanded: isExpanding });
			} );
		} );
		
		// Show the gradient color only when the header is enabled.
		wp.customize( 'grey_gradient_header', function( setting ) {
			wp.customize.control( 'gradient_color', function( control ) {
				var visibility = function() {
					if ( 'on' === setting.get() ) {
						control.container.slideDown( 180 );
					} else {
						control.container.slideUp( 180 );
					}
				};
				
				visibility();
				setting.bind( visibility );
			});
		});
		
		wp.customize( 'show_on_404', function( setting ) {
			// Show the custom 404 error page option only when 'A 404 Error' is set to 'Displays a custom page'.
			wp.customize.control( 'custom_404_page', function( control ) {
				var visibility = function() {
					if ( 'custom_page' === setting.get() ) {
						control.container.slideDown( 180 );
					} else {
						control.container.slideUp( 180 );
					}
				};
				
				visibility();
				setting.bind( visibility );
			});
			// Show the custom 404 error redirect option only when 'A 404 Error' is set to 'Redirects to another page'.
			wp.customize.control( 'custom_404_redirect', function( control ) {
				var visibility = function() {
					if ( 'redirect' === setting.get() ) {
						control.container.slideDown( 180 );
					} else {
						control.container.slideUp( 180 );
					}
				};
				
				visibility();
				setting.bind( visibility );
			});
		});
	});
  
	// Refresh the preview when the Author Pages option is changed.
	
	wp.customize( 'author_pages', function( value ) {
		value.bind( function( to ) {
			 wp.customize.previewer.refresh();
		} );
	} );
  
	// Refresh the preview when the 'A 404 Error' option is changed.
	
	wp.customize( 'show_on_404', function( value ) {
		value.bind( function( to ) {
			 wp.customize.previewer.refresh();
		} );
	} );
	
	// Refresh the preview when the custom 404 error page option is changed.
	
	wp.customize( 'custom_404_page', function( value ) {
		value.bind( function( to ) {
			 wp.customize.previewer.refresh();
		} );
	} );
	
	// Refresh the preview when the custom 404 redirect page option is changed.
	
	wp.customize( 'custom_404_redirect', function( value ) {
		value.bind( function( to ) {
			 wp.customize.previewer.refresh();
		} );
	} );
	
})( jQuery );
