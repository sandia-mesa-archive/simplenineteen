const buttons = document.querySelectorAll('.sbtn:not(.popup-trigger)');
var url = window.location.href;

buttons.forEach(trigger => {
  trigger.onclick = function(event) {
    var platform = trigger.getAttribute('class');
    if ( platform.startsWith('sbtn s-') )
      platform = platform.split('sbtn s-').pop();

    const urlPrefix = trigger.getAttribute('data-url-prefix');
    const beforeURLParam = trigger.getAttribute('data-before-url-param');
    const titleInLink = trigger.getAttribute('title-in-link');
    // Get the page title from the og:title meta tag, if it exists.
    var ogTitleData = document.querySelectorAll('meta[property="og:title"]')[0];
    if (ogTitleData) {
      var title = ogTitleData.getAttribute("content");
    } else {
      var title = document.getElementsByTagName("title")[0].innerHTML;
    }

    // Get all the hashtags in the share meta part of the header.
    var hashtags = document.querySelectorAll('meta[property="simplenineteen_share_hashtag"]');

    // Get the fediverse author.
    var author = document.querySelectorAll(`meta[property="simplenineteen_${platform}_author`);

    // Let's begin building the URL.
    var share_url = urlPrefix

    if ( titleInLink ) {
      share_url += encodeURIComponent(title);

      if (beforeURLParam) {
        share_url += beforeURLParam + encodeURIComponent(url);
      } else {
        share_url += " " + encodeURIComponent(url);
      }

      if (hashtags.length > 0 && trigger.getAttribute('hashtags')) {
        var i;
        for (i = 0; i < hashtags.length; i++) {
          var hashtagContent = hashtags[i].getAttribute('content');
          if (!hashtagContent.startsWith("#"))
            hashtagContent = "#" + hashtagContent;

          share_url += " " + encodeURIComponent(hashtagContent);
        }
      }

      if (author.length > 0 && trigger.getAttribute('authors')) {
        var i;
        for (i = 0; i < author.length; i++) {
          var authorHandle = author[i].getAttribute('content');
          share_url += " " + encodeURIComponent(authorHandle);
        }
      }

    } else {
      share_url += encodeURIComponent(url);
    }

    window.open(share_url, '_blank');
  }
})