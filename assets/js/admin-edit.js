/**
 * Script within the admin edit post/page screen.
 *
 * Contextually show the 'Show logo only option'.
 */

(function ($){
    $( "#simplenineteen-fullscreen" ).click(function() {
        if($("#simplenineteen-fullscreen").is(':checked')) {
            $( "#simplenineteen-show-logo-option").slideDown( '30' );
        } else {
            $( "#simplenineteen-show-logo-option").slideUp( '30' );
        }
    });
})(jQuery);
