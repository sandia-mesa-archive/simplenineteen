const modalTriggers = document.querySelectorAll('.popup-trigger')
const modalCloseTrigger = document.querySelector('.popup-modal__close')
const body = document.querySelector('body')
var blackoutDiv = document.createElement('div');
blackoutDiv.className = 'body-blackout';

// Get the current page's URL
var url   = window.location.href;

modalTriggers.forEach(trigger => {
  trigger.addEventListener('click', () => {
    const popupTrigger = trigger.getAttribute('data-popup-trigger');
    const urlPrefix = trigger.getAttribute('data-url-prefix');
    const beforeURLParam = trigger.getAttribute('data-before-url-param');
    const popupModal = document.querySelector(`[data-popup-modal="${popupTrigger}"]`);
    var defaultURL = localStorage[`${popupTrigger}_instance`];
    if (!defaultURL)
      defaultURL = "https://";
    document.querySelector(`.${popupTrigger}.popup-modal__instance`).defaultValue = defaultURL;

    body.appendChild(blackoutDiv);
    popupModal.classList.add('is--visible');


    popupModal.querySelector('.popup-modal__close').onclick = function(event) {
      popupModal.classList.remove('is--visible');
      body.removeChild(blackoutDiv);
    }
    window.onclick = function(event) {
      if (event.target == blackoutDiv) {
        popupModal.classList.remove('is--visible');
        body.removeChild(blackoutDiv);
      }
    }
    popupModal.querySelector('.popup-modal__submit').onclick = function(event) {
      var instance = document.querySelector(`.${popupTrigger}.popup-modal__instance`).value;
      var error = document.querySelector(`.${popupTrigger}.error`);

      if ( instance == "https://" || instance == "http://" || instance == "" ) {
        error.style.display = "block";
      } else {
        error.style.display = "none";
        // Handle URL formats
        if ( !instance.startsWith("https://") && !instance.startsWith("http://") )
          instance = "https://" + instance;

        // Get the page title from the og:title meta tag, if it exists.
        var ogTitleData = document.querySelectorAll('meta[property="og:title"]')[0];
        if (ogTitleData) {
          var title = ogTitleData.getAttribute("content");
        } else {
          var title = document.getElementsByTagName("title")[0].innerHTML;
        }

        // Handle slash
        if ( !instance.endsWith("/") )
            instance = instance + "/";

        // Cache the instance/domain for future requests (if requested)
        if (document.querySelector(`.${popupTrigger}.popup-modal__remember`).checked == true)
          localStorage[`${popupTrigger}_instance`] = instance;

        // Get all the hashtags in the share meta part of the header.
        var hashtags = document.querySelectorAll('meta[property="simplenineteen_share_hashtag"]');

        // Get the fediverse author.
        var author = document.querySelectorAll('meta[property="fediverse:author"]');

        // Now let's start building the link.
        var share_url = instance + urlPrefix + encodeURIComponent(title);

        // If we have a before URL parameter argument set, include that before the URL.
        // Otherwise, do a space between the title and the URL.
        if (beforeURLParam) {
          share_url += beforeURLParam + encodeURIComponent(url);
        } else {
          share_url += " " + encodeURIComponent(url);
        }

        if (hashtags.length > 0 && trigger.getAttribute('hashtags')) {
          var i;
          for (i = 0; i < hashtags.length; i++) {
            var hashtagContent = hashtags[i].getAttribute('content');
            if (!hashtagContent.startsWith("#"))
              hashtagContent = "#" + hashtagContent;

            share_url += " " + encodeURIComponent(hashtagContent);
          }
        }

        if (author.length > 0 && trigger.getAttribute('authors')) {
          var i;
          for (i = 0; i < author.length; i++) {
            var authorHandle = author[i].getAttribute('content');
            share_url += " " + encodeURIComponent(authorHandle);
          }
        }

        window.open(share_url, '_blank');

        document.querySelector(`[data-popup-modal="${popupTrigger}"]`).classList.remove('is--visible');
        body.removeChild(blackoutDiv);
      }

    }
  })
})